from flask import Flask, jsonify


def create_app():
    app = Flask(__name__)
    # Register blueprints here
    from app.todo.todo_routes import todo_bp
    app.register_blueprint(todo_bp)

    @app.route("/")
    def home():
        return jsonify({"message": "Welcome"})

    return app
