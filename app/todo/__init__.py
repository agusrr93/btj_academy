from flask import Blueprint

todo_bp = Blueprint('todo', __name__)

# Import routes to register them
from . import todo_routes
