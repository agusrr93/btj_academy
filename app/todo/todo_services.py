from .todo_model import Todo


class TodoService:
    todos = []

    @classmethod
    def get_all_todos(cls):
        return [todo.__dict__ for todo in cls.todos]

    @classmethod
    def get_todo_by_id(cls, todo_id):
        for todo in cls.todos:
            if todo.todo_id == todo_id:
                return todo.__dict__
        return None

    @classmethod
    def create_todo(cls, data):
        todo_id = len(cls.todos) + 1
        todo = Todo(todo_id, data['title'], data.get('completed', False))
        cls.todos.append(todo)
        return todo.__dict__

    @classmethod
    def update_todo(cls, todo_id, data):
        for todo in cls.todos:
            if todo.todo_id == todo_id:
                todo.title = data.get('title', todo.title)
                todo.completed = data.get('completed', todo.completed)
                return todo.__dict__
        return None

    @classmethod
    def delete_todo(cls, todo_id):
        for todo in cls.todos:
            if todo.todo_id == todo_id:
                cls.todos.remove(todo)
                return True
        return False
