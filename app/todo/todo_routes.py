from flask import request, jsonify
from . import todo_bp
from .todo_services import TodoService


@todo_bp.route('/todos', methods=['GET'])
def get_todos():
    todos = TodoService.get_all_todos()
    return jsonify(todos)


@todo_bp.route('/todos', methods=['POST'])
def create_todo():
    data = request.get_json()
    todo = TodoService.create_todo(data)
    return jsonify(todo), 201


@todo_bp.route('/todos/<int:todo_id>', methods=['GET'])
def get_todo(todo_id):
    todo = TodoService.get_todo_by_id(todo_id)
    if todo:
        return jsonify(todo)
    return jsonify({'message': 'Todo not found'}), 404


@todo_bp.route('/todos/<int:todo_id>', methods=['PUT'])
def update_todo(todo_id):
    data = request.get_json()
    todo = TodoService.update_todo(todo_id, data)
    if todo:
        return jsonify(todo)
    return jsonify({'message': 'Todo not found'}), 404


@todo_bp.route('/todos/<int:todo_id>', methods=['DELETE'])
def delete_todo(todo_id):
    success = TodoService.delete_todo(todo_id)
    if success:
        return jsonify({'message': 'Todo deleted successfully'})
    return jsonify({'message': 'Todo not found'}), 404
