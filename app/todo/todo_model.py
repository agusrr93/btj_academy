class Todo:
    def __init__(self, todo_id, title, completed=False):
        self.todo_id = todo_id
        self.title = title
        self.completed = completed
