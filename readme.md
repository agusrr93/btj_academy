# Your Project Name

Description of your project.

## Installation

1. Clone the repository:

    ```bash
    git clone <repository_url>
    ```

2. Navigate to the 'api' folder:

    ```bash
    cd flask_api
    ```

3. Activate the virtual environment:
    - For Windows:
        ```bash
        python3 -m venv venv
        venv\Scripts\activate
        ```
    - For Mac or Linux:
        ```bash
        python3 -m venv venv
        source venv/bin/activate
        ```

    To deactivate the virtual environment, use:
    ```bash
    deactivate
    ```

4. Install dependencies:

    ```bash
    pip3 install -r requirements.txt
    ```

## Usage

Run the following command to start the server using Gunicorn:

```bash
gunicorn -w 4 run:app
